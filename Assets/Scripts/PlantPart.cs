using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlantPart : MonoBehaviour
{
    public PlayerController owner;
    public int energy;
    public HexDirections direction;
    public Text energyText;
    public Tile tile;
    public PlantPart parent;
    public List<PlantPart> children;
    public float energyPerTick = 1;

    // Start is called before the first frame update
    void Start()
    {
        children = new List<PlantPart>();        
    }

    // Update is called once per frame
    void Update()
    {
        UpdateUI();
    }

    public void UpdateUI ()
    {
        energyText.text = ""+energy;
    }

    public virtual void EnergizePlant(PlayerController energizer, int amount = 1)
    {
        // If the energizer is us
        if (energizer == owner)
        {
            // Debug.Log(name + "is energizing");
            energy += amount;
            Mathf.Clamp(energy, 0, 100);
        }
        else if (owner != null)
        {
            // Otherwise, someone else is energizing us / attacking
            // Debug.Log(name + " is contested");
            energy -= amount;

            // If they kill us, die
            if (energy < 0)
            {
                Die();
                // Kill Children
                foreach (PlantPart child in children)
                {
                    child.Die();
                }
            }
            else
            {
                // DO Nothing
            }
        }
        else
        {
            // No one owns this plant? Kill it
            Die();
        }
        
    }

    public void Die()
    {
        // Destroy the plant part and set the tile to no one
        Debug.Log(tile.name + "|" +tile.plant.name+ " died");
        tile.TakeOwnership(null);
        Destroy(this.gameObject);
    }

    public void EnergizeNeighbors (Tile tile)
    {
        //Energize all the neighbors
        Tile neighbor;
        neighbor = GameManager.instance.mapGenerator.GetNeighborTile(tile, HexDirections.NE);
        if(neighbor!=null) neighbor.plant.EnergizePlant(neighbor.owner);

        neighbor = GameManager.instance.mapGenerator.GetNeighborTile(tile, HexDirections.NW);
        if (neighbor != null) neighbor.plant.EnergizePlant(neighbor.owner);

        neighbor = GameManager.instance.mapGenerator.GetNeighborTile(tile, HexDirections.SE);
        if (neighbor != null) neighbor.plant.EnergizePlant(neighbor.owner);

        neighbor = GameManager.instance.mapGenerator.GetNeighborTile(tile, HexDirections.SW);
        if (neighbor != null) neighbor.plant.EnergizePlant(neighbor.owner);

        neighbor = GameManager.instance.mapGenerator.GetNeighborTile(tile, HexDirections.E);
        if (neighbor != null) neighbor.plant.EnergizePlant(neighbor.owner);

        neighbor = GameManager.instance.mapGenerator.GetNeighborTile(tile, HexDirections.W);
        if (neighbor != null) neighbor.plant.EnergizePlant(neighbor.owner);

    }


    public void Grow (HexDirections directiontoGrow)
    {
        // If we aren't given a direction, quit
        if (directiontoGrow == HexDirections.None)
        {
            EnergizePlant(owner, (int)energyPerTick);
            return;
        }

        // If we have no energy, quit
        if ( energy <= 0 )
        {
            return;
        }

        // Get the next tile
        Tile nextTile = GameManager.instance.mapGenerator.GetNeighborTile(this.tile, directiontoGrow);

        // If the next tile exists
        if (nextTile != null)
        {
            // If we own the next spot
            if (nextTile.owner == owner)
            {
                // Energize it??
                nextTile.plant.EnergizePlant(owner);
                // Remove energy from ourselves
                energy--;
                // Stop growing in a direction 
            }

            // if no one owns it...
            else if (nextTile.owner == null)
            {
                // If it is not a rock...
                if (GameManager.instance.mapGenerator.IsRock(nextTile) == false)
                {
                    // If we have the energy 
                    if (energy > 0)
                    {
                        // Take over the tile (with the owner of THIS tile)
                        nextTile.TakeOwnership(owner);
                        // Create a vine there
                        nextTile.PlaceVine(directiontoGrow);
                        // Save the parent of this new vine
                        nextTile.plant.parent = this;
                        children.Add(nextTile.plant);
                        // Reset that tile's timer
                        nextTile.countdown = tile.secondsBetweenGrows;
                    }
                }
                else
                {
                    // it is a rock, so Do Nothing
                    //Do Nothing
                }
            }
            else
            {
                // TODO: An enemy owns it, fight!!!
                nextTile.plant.EnergizePlant(owner, energy);
                // Set your energy to 0
                energy = 0;
            }
        }
        else
        {
            // Next tile doesn't exist?
            // Energize  
            EnergizePlant(owner);
        }
    }

}
