using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;
    public MapGenerator mapGenerator;

    public PlayerController playerOne;
    public PlayerController playerTwo;
    [Header("Data")]
    public float globalCooldown = 0.2f;
    public float startTimeBetweenGrows = 1.5f;

    [Header("Objects")]
    public Material unownedTileMaterial;
    public RootBall rootPrefab;
    public Vine vinePrefab;

    void Awake()
    {
        if (instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

}

