using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum HexDirections { NE, E, SE, SW, W, NW, None };
