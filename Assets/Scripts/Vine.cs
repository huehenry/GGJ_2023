using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Vine : PlantPart
{
    public void Rotate( HexDirections direction)
    {
        switch (direction) {
            case HexDirections.NE:
                transform.Rotate(0, 30, 0);
                break;
            case HexDirections.E:
                transform.Rotate(0, 90, 0);
                break;
            case HexDirections.SE:
                transform.Rotate(0, 150, 0);
                break;
            case HexDirections.SW:
                transform.Rotate(0, 210, 0);
                break;
            case HexDirections.W:
                transform.Rotate(0, 270, 0);
                break;
            case HexDirections.NW:
                transform.Rotate(0, 330, 0);
                break;
        }
    }

 
}
