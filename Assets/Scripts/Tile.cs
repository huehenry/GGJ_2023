using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tile : MonoBehaviour
{
    public PlantPart plant;
    public PlayerController owner;
    //---
    public MeshRenderer meshRend;
    public Vector2 tileLocation;
    [Header("Time Variables")]
    public float secondsBetweenGrows;
    public float countdown;

    // Start is called before the first frame update
    void Awake()
    {
        meshRend = GetComponent<MeshRenderer>();
    }

    void Start()
    {
        secondsBetweenGrows = GameManager.instance.startTimeBetweenGrows;
        countdown = secondsBetweenGrows;
    }

    // Update is called once per frame
    void Update()
    {
        // Countdown the timer
        countdown -= Time.deltaTime;
        if (countdown <= 0)
        {
            // If someone owns this tile
            if (owner != null) {
                // If there is a plant here
                if (plant != null)
                {
                    // Grow
                    GrowVine(plant.direction);
                }
            }

            // Reset countdown
            countdown = secondsBetweenGrows;
        }
    }

    public void GrowVine ( HexDirections direction )
    {
        plant.Grow(direction);
    }

    public void PlaceVine ( HexDirections direction)
    {
        // For now, spawn only here
        Tile tileToSpawnAt = this;

        // Can't place vine if no direction
        if (direction == HexDirections.None)
        {
            Debug.Log("ERROR: Tried to place a vine with no direction.");
            return;
        }

        // Spawn vine at THIS tile
        Vine newVine = Instantiate(GameManager.instance.vinePrefab, tileToSpawnAt.transform.position, Quaternion.identity);

        // Connect this tile in the plant
        newVine.tile = tileToSpawnAt;

        // Rotate in the right direction
        newVine.direction = direction;
        newVine.Rotate(direction);

        // Start energy at zero
        newVine.energy = 0;

        // Save owner of plant
        newVine.owner = tileToSpawnAt.owner;

        // Vines don't gain energy on tick
        newVine.energyPerTick = 0;

        // Save vine in this tile
        plant = newVine;
    }


    public void PlaceRoot ()
    {
        // Place root object and Save that this tile has it         
        plant = Instantiate(GameManager.instance.rootPrefab, transform.position, Quaternion.identity) as PlantPart;
        plant.owner = owner;
        plant.direction = HexDirections.None;
        plant.tile = this;
        plant.energy = 0;
        plant.parent = null; // Roots have no parents, so you can't kill parents
    }


    public void TakeOwnership( PlayerController player )
    {
        if (player == null)
        {
            owner = null;
            Material[] mats = meshRend.materials;
            for (int i = 0; i < mats.Length; i++)
            {
               mats[i].color = GameManager.instance.unownedTileMaterial.color;
            }
        }
        else
        {
            owner = player;
            Material[] mats = meshRend.materials;
            for (int i=0; i<mats.Length; i++)
            {
               mats[i].color = player.color;
            }
        }
    }

}
