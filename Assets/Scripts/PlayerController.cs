using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : Controller
{
    public Color color;
    public int selectedTileX = 0;
    public int selectedTileY = 0;
    public int selectedAbilityNumber = 0;
    public Light playerLight;

    [Header("Control Strings")]
    public string horizontal;
    public string vertical;
    public string nextPower;
    public string prevPower;
    public string submit;
    [Header("Abilities")]
    public List<Upgrade> abilities;
    //--
    private float nextInputTime;
    public bool isInvertedX;
    public bool isInvertedY;

    public UIPanel playerUI;


    // Start is called before the first frame update
    void Start()
    {
        
        // Set light to player color
        playerLight.color = color;
        nextInputTime = Time.time + GameManager.instance.globalCooldown;
    }

    // Update is called once per frame
    void Update()
    {
        // Move
        MoveTileSelection();
        // Change Ability if Needed
        ProcessUIInput();
        // Process Action
        ProcessAction();
        // Adjust UI as needed
        UpdateUI();

    }

    public void ProcessAction()
    {
        Tile currentTile = GameManager.instance.mapGenerator.map[selectedTileX, selectedTileY].GetComponent<Tile>();

        if (Input.GetButtonDown(submit))
        { 
            switch (selectedAbilityNumber)
            {
                case 0:
                    // Pressed Grow Root
                    // Check if we are our vine
                    if (isOurVine(currentTile))
                    {
                        AudioManager.instance.PlaySound("Submit");
                        // Destroy the vine
                        Destroy(currentTile.plant.gameObject);
                        currentTile.PlaceRoot();
                    }
                    else
                    {
                        // Otherwise, play error sound
                        AudioManager.instance.PlaySound("Error");
                    }
                    break;
                case 1:
                    // Pressed Grow Root NW
                    // Check if we are our root
                    if (isOurRoot(currentTile))
                    {
                        AudioManager.instance.PlaySound("Submit");
                        currentTile.plant.direction = HexDirections.NW;
                        //Debug.Log("Grow NorthWest");
                    }
                    else
                    {
                        // Otherwise, play error sound
                        AudioManager.instance.PlaySound("Error");
                    }
                    break;
                case 2:
                    // Pressed Grow Root NE
                    // Check if we are our root
                    if (isOurRoot(currentTile))
                    {
                        AudioManager.instance.PlaySound("Submit");
                        currentTile.plant.direction = HexDirections.NE;
                        //Debug.Log("Grow NorthEast");
                    }
                    else
                    {
                        // Otherwise, play error sound
                        AudioManager.instance.PlaySound("Error");
                    }
                    break;
                case 3:
                    // Pressed Grow Root E
                    // Check if we are our root
                    if (isOurRoot(currentTile))
                    {
                        AudioManager.instance.PlaySound("Submit");
                        currentTile.plant.direction = HexDirections.E;
                        //Debug.Log("Grow East");
                    }
                    else
                    {
                        // Otherwise, play error sound
                        AudioManager.instance.PlaySound("Error");
                    }
                    break;
                case 4:
                    // Pressed Grow Root SE
                    // Check if we are our root
                    if (isOurRoot(currentTile))
                    {
                        AudioManager.instance.PlaySound("Submit");
                        currentTile.plant.direction = HexDirections.SE;
                        //Debug.Log("Grow SouthEast");
                    }
                    else
                    {
                        // Otherwise, play error sound
                        AudioManager.instance.PlaySound("Error");
                    }
                    break;
                case 5:
                    // Pressed Grow Root SW
                    // Check if we are our root
                    if (isOurRoot(currentTile))
                    {
                        AudioManager.instance.PlaySound("Submit");
                        currentTile.plant.direction = HexDirections.SW;
                        //Debug.Log("Grow SouthWest");
                    }
                    else
                    {
                        // Otherwise, play error sound
                        AudioManager.instance.PlaySound("Error");
                    }
                    break;
                case 6:
                    // Pressed Grow Root W
                    // Check if we are our root
                    if (isOurRoot(currentTile))
                    {
                        AudioManager.instance.PlaySound("Submit");
                        currentTile.plant.direction = HexDirections.W;
                        //Debug.Log("Grow West");
                    }
                    else
                    {
                        // Otherwise, play error sound
                        AudioManager.instance.PlaySound("Error");
                    }
                    break;
                case 7:
                    // TODO: Add flower
                    // For now, play error sound
                    AudioManager.instance.PlaySound("Error");
                    break;
            }
        }
    }


    public void ProcessUIInput()
    {
        if (Input.GetButtonDown(nextPower))
        {
            playerUI.LightUp(selectedAbilityNumber, false);
            selectedAbilityNumber++;
            selectedAbilityNumber = Mathf.Clamp(selectedAbilityNumber, 0, abilities.Count - 1);
            AudioManager.instance.PlaySound("ChangeAbility");

        }
        if (Input.GetButtonDown(prevPower))
        {
            playerUI.LightUp(selectedAbilityNumber, false);
            selectedAbilityNumber--;
            selectedAbilityNumber = Mathf.Clamp(selectedAbilityNumber, 0, abilities.Count - 1);
            AudioManager.instance.PlaySound("ChangeAbility");
        }

    }

    public bool isOurs(Tile tile)
    {
        if (tile.owner == this)
        {
            return true;
        }
        return false;
    }

    public bool isOurVine( Tile tile)
    {
        if (isOurs(tile)) {
            if (GameManager.instance.mapGenerator.IsVine(tile))
            {
                return true;
            }
        }
        return false;
    }

    public bool isOurRoot(Tile tile)
    {
        // If we own the tile
        if (isOurs(tile))
        {
            // If there is a plant here 
            if (GameManager.instance.mapGenerator.IsRootBall(tile))
            {
                return true;
            }
        }
        return false;
    }

    public bool isOurFlower(Tile tile)
    {
        // If we own the tile
        if (isOurs(tile))
        {
            // If there is a plant here 
            if (GameManager.instance.mapGenerator.IsFlower(tile))
            {
                return true;
            }
        }
        return false;
    }

    public bool IsRock (Vector2 tilePosition )
    {
        return GameManager.instance.mapGenerator.IsRock(tilePosition);
    }

    public bool IsRock (Tile tile)
    {
        return GameManager.instance.mapGenerator.IsRock(tile);
     }


    public void UpdateUI()
    {
        if (isOurRoot(GameManager.instance.mapGenerator.map[selectedTileX, selectedTileY].GetComponent<Tile>())) 
        {
            // Grey out the Rootball
            if (selectedAbilityNumber == 0) {
                playerUI.RedOut(0);
            }
            else
            {
                playerUI.GreyOut(0);
            }

            // Light up our grow abilities IF we don't have a direction
            if (GameManager.instance.mapGenerator.GetTile(new Vector2((int)selectedTileX, (int)selectedTileY)).plant.direction == HexDirections.None)
            {
                for (int i = 1; i < 7; i++)
                {
                    if (selectedAbilityNumber == i)
                    {
                        playerUI.LightUp(i, true);
                    }
                    else
                    {
                        playerUI.LightUp(i, false);
                    }
                }
            } else
            {
                // Otherwise, grey them out
                for (int i = 1; i < 7; i++)
                {
                    playerUI.GreyOut(i);
                }
                //Then red out our selection
                playerUI.RedOut(selectedAbilityNumber);

            }

            // RedOut flower
            // TODO: Change when flower exists
            if (selectedAbilityNumber == 7)
            {
                playerUI.RedOut(7);
            }
            else
            {
                playerUI.GreyOut(7);
            }

        } else if ( isOurVine(GameManager.instance.mapGenerator.map[selectedTileX, selectedTileY].GetComponent<Tile>()))
        {
            // Light up root
            if (selectedAbilityNumber == 0)
            {
                playerUI.LightUp(0, true);
            }
            else
            {
                playerUI.LightUp(0, false);
            }

            // Change our grow abilities
            for (int i = 1; i < 7; i++)
            {
                if (selectedAbilityNumber == i)
                {
                    playerUI.RedOut(i);
                }
                else
                {
                    playerUI.GreyOut(i);
                }
            }

            // RedOut flower
            // TODO: Change when flower exists
            if (selectedAbilityNumber == 7)
            {
                playerUI.RedOut(7);
            }
            else
            {
                playerUI.GreyOut(7);
            }
        }
        else
        {
            if (IsRock(GameManager.instance.mapGenerator.map[selectedTileX, selectedTileY].GetComponent<Tile>()))
            {
                //Debug.Log("ROCK");
            }

            // Grey out everything
            for (int i = 0; i < 8; i++)
            {
                playerUI.GreyOut(i);
            }
            //Then red out our selection
            playerUI.RedOut(selectedAbilityNumber);
        }

    }

    public void PlaceSelection ( Vector2 tilePosition )
    {
        playerLight.transform.position =
             GameManager.instance.mapGenerator.map[(int)tilePosition.x, (int)tilePosition.y].transform.position
                + (Vector3.up * 3);

        selectedTileX = (int)tilePosition.x;
        selectedTileY = (int)tilePosition.y;
    }




    public  void MoveN ()
    {
        if ( selectedTileY < GameManager.instance.mapGenerator.map.GetLength(1) - 1)
        {
            selectedTileY += 1;
            PlaceSelection(new Vector2(selectedTileX, selectedTileY));
            AudioManager.instance.PlaySound("Move");
        }
    }

    public void MoveS()
    {
        if (selectedTileY > 0)
        {
            selectedTileY -= 1;
            PlaceSelection(new Vector2(selectedTileX, selectedTileY));
            AudioManager.instance.PlaySound("Move");

        }
    }

    public void MoveNE ()
    {
        if (selectedTileX < GameManager.instance.mapGenerator.map.GetLength(0) && 
               selectedTileY < GameManager.instance.mapGenerator.map.GetLength(1) - 1)
        {
            selectedTileY += 1;
            PlaceSelection(new Vector2(selectedTileX, selectedTileY));

        }
    }

    public void MoveNW()
    {
        if (selectedTileX > 0 &&
       selectedTileY < GameManager.instance.mapGenerator.map.GetLength(1) - 1)
        {
            selectedTileX -= 1;
            selectedTileY += 1;
            PlaceSelection(new Vector2(selectedTileX, selectedTileY));

        }
    }

    public void MoveSE()
    {
        if (selectedTileX < GameManager.instance.mapGenerator.map.GetLength(0) &&
       selectedTileY >0 )
        {
            selectedTileY -= 1;
            PlaceSelection(new Vector2(selectedTileX, selectedTileY));

        }
    }

    public void MoveSW()
    {
        if (selectedTileX > 0 &&
       selectedTileY >0 )
        {
            selectedTileX -= 1;
            selectedTileY -= 1;
            PlaceSelection(new Vector2(selectedTileX, selectedTileY));

        }
    }

    public void MoveE()
    {
        if (selectedTileX < GameManager.instance.mapGenerator.map.GetLength(0)-1 )
        {
            selectedTileX += 1;
            PlaceSelection(new Vector2(selectedTileX, selectedTileY));
            AudioManager.instance.PlaySound("Move");

        }
    }

    public void MoveW()
    {
        if (selectedTileX > 0)
        {
            selectedTileX -= 1;
            PlaceSelection(new Vector2(selectedTileX, selectedTileY));
            AudioManager.instance.PlaySound("Move");

        }
    }

    public void MoveTileSelection ( )
    {
        float deltaX = Input.GetAxis(horizontal);
        float deltaY = Input.GetAxis(vertical);

        if (isInvertedX) deltaX = -deltaX;
        if (isInvertedY) deltaY = -deltaY;

        if (Time.time >= nextInputTime)
        {
            if (deltaY < 0)
            {
                MoveS();
                nextInputTime = Time.time + GameManager.instance.globalCooldown;
            }
            else if (deltaY > 0)
            {
                MoveN();
                nextInputTime = Time.time + GameManager.instance.globalCooldown;
            }

            if (deltaX < 0)
            {
                MoveW();
                nextInputTime = Time.time + GameManager.instance.globalCooldown;
            }

            else if (deltaX > 0)
            {
                MoveE();
                nextInputTime = Time.time + GameManager.instance.globalCooldown;
            }

        }

    }



}
