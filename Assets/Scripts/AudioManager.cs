using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class AudioManager : MonoBehaviour
{
    public static AudioManager instance;
    [HideInInspector] public AudioSource source;
    public List<AudioFile> sounds;

    void Awake()
    {
        if (instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }

        source = GetComponent<AudioSource>();
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void PlaySound(AudioClip sound)
    {
        source.PlayOneShot(sound);
    }

    public void PlaySound(string soundName)
    {
       List<AudioClip> audioClips = new List<AudioClip>();

        for (int i = 0; i < sounds.Count; i++)
        {
            if (sounds[i].name == soundName)
            {
                audioClips.Add(sounds[i].clip);
            }
        }
        if (audioClips.Count > 0) {
            PlaySound(audioClips[Random.Range(0, audioClips.Count)]);
        } 
    }
}

[System.Serializable]
public struct AudioFile
{
    public string name;
    public AudioClip clip;
}