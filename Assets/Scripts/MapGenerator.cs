using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapGenerator : MonoBehaviour
{
    [Header("Tiles")]
    public List<GameObject> growableTiles;
    public List<GameObject> rockTiles;
    [Header("Data")]
    public int mapSeed;
    public int numCols;
    public int numRows;
    public float distanceBetweenCols;
    public float distanceBetweenRows;
    public float numberOfRocks;
    public GameObject[,] map; 
    //---
    private bool isOffsetRow = true;


    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }


    public bool isEven (float num)
    {
        if (num % 2 == 0)
        {
            return true;
        }
        return false;
    }

    public Vector2 GetNeighbor ( Vector2 locationToCheck, HexDirections direction )
    {
        Vector2 neighborToTest = new Vector2();
        switch (direction)
        {
            case HexDirections.NE:
                if (isEven(locationToCheck.y))
                {
                    neighborToTest = new Vector2(locationToCheck.x + 1, locationToCheck.y + 1);
                } else
                {
                    neighborToTest = new Vector2(locationToCheck.x, locationToCheck.y + 1);
                }
                break;
            case HexDirections.E:
                 neighborToTest = new Vector2(locationToCheck.x + 1, locationToCheck.y);
                break;
            case HexDirections.SE:
                if (isEven(locationToCheck.y))
                {
                    neighborToTest = new Vector2(locationToCheck.x + 1, locationToCheck.y - 1);
                }
                else
                {
                    neighborToTest = new Vector2(locationToCheck.x, locationToCheck.y - 1);
                }
                break;
            case HexDirections.SW:
                if (isEven(locationToCheck.y))
                {
                    neighborToTest = new Vector2(locationToCheck.x, locationToCheck.y - 1);
                }
                else
                {
                    neighborToTest = new Vector2(locationToCheck.x - 1, locationToCheck.y - 1);
                }
                break;
            case HexDirections.W:
                 neighborToTest = new Vector2(locationToCheck.x - 1, locationToCheck.y);
                break;
            case HexDirections.NW:
                if (isEven(locationToCheck.y))
                {
                    neighborToTest = new Vector2(locationToCheck.x, locationToCheck.y + 1);
                }
                else
                {
                    neighborToTest = new Vector2(locationToCheck.x - 1 , locationToCheck.y + 1);
                }
                break;
        }
        if (isValid(neighborToTest))
        {
            return neighborToTest;
        } else
        {
            return new Vector2(Mathf.Infinity, Mathf.Infinity);
        }
    }

    public Tile GetTile ( Vector2 self )
    {
        return map[(int)self.x, (int)self.y].GetComponent<Tile>();
    }

    public Tile GetNeighborTile(Tile self, HexDirections direction)
    {
        Vector2 neighborToCheck = GetNeighbor(self.tileLocation, direction);
        if (isValid(neighborToCheck))
        {
            return map[(int)neighborToCheck.x, (int)neighborToCheck.y].GetComponent<Tile>();
        }
        else
        {
            return null;
        }
    }


    public Tile GetNeighborTile (Vector2 self, HexDirections direction )
    {
        Vector2 neighborToCheck = GetNeighbor(self, direction);
        if (isValid(neighborToCheck))
        {
            return map[(int)neighborToCheck.x, (int)neighborToCheck.y].GetComponent<Tile>();
        } else
        {
            return null;
        }  
    }

    public bool isValid (Vector2 tileToCheck)
    {
        if (tileToCheck.x < 0 || tileToCheck.y < 0 || tileToCheck.x >= numCols || tileToCheck.y>= numRows)
        {
            return false;
        }
        return true;
    }

    public void GenerateMap()
    {
        map = new GameObject[numCols, numRows];

        // Seed map if needed
        if (mapSeed >0)
        { 
            Random.InitState(mapSeed); 
        }

        float zPosition = 0;
        float xPosition = 0;

        // For each row in our map
        for (int currentRow = 0; currentRow < numRows; currentRow++)
        {
            // Set my Z position value
            zPosition += distanceBetweenRows;

            // If we are an offset row, offset, otherwise start at 0
            if (isOffsetRow) {
                xPosition = distanceBetweenCols * 0.5f;
            } else
            {
                xPosition = 0;
            }
            isOffsetRow = !isOffsetRow;

            // For each col in that row
            for (int currentCol = 0; currentCol < numCols; currentCol++)
            {
                // Set our X position value
                xPosition += distanceBetweenCols;


                // Generate a random growableTile from the list
                GameObject mapTileObject = Instantiate(growableTiles[Random.Range(0, growableTiles.Count)], new Vector3(xPosition, 0, zPosition), Quaternion.identity) as GameObject;
                map[currentCol, currentRow] = mapTileObject;
                mapTileObject.transform.parent = transform;
                mapTileObject.name = "Tile ("+currentCol+","+currentRow+")";
                mapTileObject.GetComponent<Tile>().tileLocation = new Vector2(currentCol, currentRow);
                mapTileObject.GetComponent<Tile>().TakeOwnership(null);
            } 
        }

        // For each number of rocks - go find a random spot and swap with a rock.
        for (int rockNum = 0; rockNum < numberOfRocks; rockNum++)
        {
            // Choose a random spot on the existing map
            int randomX = Random.Range(0, numCols);
            int randomY = Random.Range(0, numRows);
            // Save its position
            Vector3 placeToPutRock = map[randomX, randomY].transform.position;
            // Destroy the tile currently there
            Destroy(map[randomX, randomY].gameObject);
            // Add a rock there instead
            GameObject mapTileObject = Instantiate(rockTiles[Random.Range(0, rockTiles.Count)], placeToPutRock, Quaternion.identity) as GameObject;
            map[randomX, randomY] = mapTileObject;            
            mapTileObject.name = "Tile (" + randomX + "," + randomY + ")";
            mapTileObject.GetComponent<Tile>().tileLocation = new Vector2(randomX, randomY);
            mapTileObject.transform.parent = transform;
            mapTileObject.GetComponent<Tile>().TakeOwnership(null);
        }


        int maxXForPlayer1 = numCols / 2;
        int maxYForPlayer1 = numRows / 2;

        int randomX1 = Random.Range(0, maxXForPlayer1);
        int randomY1 = Random.Range(0, maxYForPlayer1);

        int randomX2 = Random.Range(maxXForPlayer1 + 1, numCols);
        int randomY2 = Random.Range(maxYForPlayer1 + 1, numCols);

        // Place Player
        map[randomX1, randomY1].GetComponent<Tile>().TakeOwnership(GameManager.instance.playerOne);
        map[randomX2, randomY2].GetComponent<Tile>().TakeOwnership(GameManager.instance.playerTwo);

        // Move player selection
        GameManager.instance.playerOne.PlaceSelection(new Vector2(randomX1, randomY1));
        GameManager.instance.playerTwo.PlaceSelection(new Vector2(randomX2, randomY2));


        // Place a root node for players
        map[randomX1, randomY1].GetComponent<Tile>().TakeOwnership(GameManager.instance.playerOne);
        map[randomX1, randomY1].GetComponent<Tile>().PlaceRoot();
        map[randomX2, randomY2].GetComponent<Tile>().TakeOwnership(GameManager.instance.playerTwo);
        map[randomX2, randomY2].GetComponent<Tile>().PlaceRoot();


        // Move our camera
        Vector3 camPos = Camera.main.transform.position;
        Camera.main.transform.position = camPos + new Vector3(((numCols * distanceBetweenCols) / 2 ) +0.5f, 0, ((numRows * distanceBetweenRows) / 2));

    
    }

    public bool IsRootBall(Tile tile)
    {
        // If there is a plant here 
        if (tile.plant.GetType() == typeof(RootBall))
        {
            return true;
        }
        return false;
    }

    public bool IsVine(Tile tile)
    {
        // If there is a plant here 
        if (tile.plant.GetType() == typeof(Vine))
        {
            return true;
        }
        return false;
    }


    public bool IsFlower(Tile tile)
    {
        // If there is a plant here 
        if (tile.plant.GetType() == typeof(Flower))
        {
            return true;
        }
        return false;
    }

    public bool IsRock (Vector2 tileLocation)
    {
        Tile tile = map[(int)tileLocation.x, (int)tileLocation.y].GetComponent<Tile>();
        return IsRock(tile);
    }

    public bool IsRock(Tile tile)
    {
        // If there is a plant here 
        if (tile.GetType() == typeof(TileRock))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}