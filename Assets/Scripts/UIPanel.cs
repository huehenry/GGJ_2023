using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIPanel : MonoBehaviour
{
    public List<Image> UIButtons;
    public Image panelBackground;
    public PlayerController owner;

    // Start is called before the first frame update
    void Start()
    {
        // Tell the owner that it owns us
        owner.playerUI = this;
        panelBackground.color = new Color(owner.color.r, owner.color.g, owner.color.b, 0.3f);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void LightUp ( int actionNumberToLight, bool isLightUp = true )
    {
        if (isLightUp)
        {
            UIButtons[actionNumberToLight].color = Color.yellow;
        } else
        {
            UIButtons[actionNumberToLight].color = Color.white;
        }
    }

    public void RedOut (int actionNumberToLight)
    {
        UIButtons[actionNumberToLight].color = Color.red;
    }

    public void GreyOut (int actionNumberToLight )
    {
        UIButtons[actionNumberToLight].color = Color.grey;
    }


}
